<?php
  include 'koneksi.php';
  $arr_periode = array(
    "Januari" => "Januari",
    "Februari" => "Februari",
    "Maret" => "Maret",
    "April" => "April",
    "Mei" => "Mei",
    "Juni" => "Juni",
    "Juli" => "Juli",
    "Agustus" => "Agustus",
    "September" => "September",
    "Oktober" => "Oktober",
    "November" => "November",
    "Desember" => "Desember");

  $func = $_GET['aksi'];
  $datas = $_POST['datas'];

  switch ($func) {
    case 'periode_bayar':
      get_periode_pembayaran($datas, $koneksi, $arr_periode);
      break;
    case 'jumlah_bayar':
      get_jumlah_bayar($datas, $koneksi);
      break;
    case 'chart_bar_admin':
      get_chart_admin($datas, $koneksi);
      break;

    default:
      # code...
      break;
  }

  function get_periode_pembayaran($data, $koneksi, $arr_periode){
    $result = mysqli_query($koneksi, "SELECT periode FROM tb_pembayaran p
      INNER JOIN tb_tahun_ajaran t
      ON p.id_tahun_ajaran = t.id_tahun_ajaran
      WHERE p.jumlah_bayar >= t.biaya_iuran_komite
      AND nis = '".$data['nis']."' AND p.id_tahun_ajaran = '".$data['id_tahun_ajaran']."'");

    $res[0] = '';
    while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
      $res[] = $row['periode'];
    }

    $html = '<select class="form-control select2" name="periode" id="periode" data-placeholder="Pilih Periode" style="width: 100%">
      <option value=""></option>';
    foreach ($arr_periode as $val) {
      if (!in_array($val, $res)) {
        $html .= '<option value="'.$val.'">'.$val.'</option>';
      }
    }
    $html .= '</select>';
    echo $html;
  }

  function get_jumlah_bayar($data, $koneksi){
    $result = mysqli_query($koneksi, "SELECT IFNULL(
      (SELECT biaya_iuran_komite - jumlah_bayar FROM tb_pembayaran p
      INNER JOIN tb_tahun_ajaran t
      ON p.id_tahun_ajaran = t.id_tahun_ajaran
      WHERE nis = '".$data['nis']."' AND p.id_tahun_ajaran = '".$data['id_tahun_ajaran']."' AND periode = '".$data['periode']."')
      ,
      (SELECT biaya_iuran_komite FROM tb_tahun_ajaran
      WHERE id_tahun_ajaran = '".$data['id_tahun_ajaran']."')
      ) AS biaya");

    $res = mysqli_fetch_array($result, MYSQLI_ASSOC);
    echo (int)$res['biaya'];
  }

  function get_chart_admin($datas, $koneksi){
    $arr_periode_gan = array(
      "Juli" => "Juli",
      "Agustus" => "Agustus",
      "September" => "September",
      "Oktober" => "Oktober",
      "November" => "November",
      "Desember" => "Desember");
    $arr_periode_gen = array(
      "Januari" => "Januari",
      "Februari" => "Februari",
      "Maret" => "Maret",
      "April" => "April",
      "Mei" => "Mei",
      "Juni" => "Juni");
    $loop_siswa = mysqli_query($koneksi, "SELECT * FROM tb_siswa s ".$datas['where_tambahan']);

    while ($row_siswa = mysqli_fetch_array($loop_siswa, MYSQLI_ASSOC)){
      $loop_th_ajaran = mysqli_query($koneksi, "SELECT * FROM tb_tahun_ajaran WHERE id_tahun_ajaran = '".$datas['id_tahun_ajaran']."'");
      while ($row_th_ajaran = mysqli_fetch_array($loop_th_ajaran, MYSQLI_ASSOC)){
        $piut_sisa = "";
        $loop_piutang = mysqli_query($koneksi, "SELECT periode, p.jumlah_bayar
          FROM tb_pembayaran p
          INNER JOIN tb_tahun_ajaran t
          ON p.id_tahun_ajaran = t.id_tahun_ajaran
          AND nis = '".$row_siswa['nis']."' AND t.id_tahun_ajaran = '".$row_th_ajaran['id_tahun_ajaran']."'");
        while ($row_piutang = mysqli_fetch_array($loop_piutang, MYSQLI_ASSOC)) {
          $piut_sisa[$row_piutang['periode']] = $row_piutang['jumlah_bayar'];
        }

        $counter_semester = '';
        if ($row_th_ajaran['semester']=='Ganjil') {
          $counter_semester = $arr_periode_gan;
        }else{
          $counter_semester = $arr_periode_gen;
        }

        foreach ($counter_semester as $key => $val) {
          $sisa = $row_th_ajaran['biaya_iuran_komite'];
          if (!empty($piut_sisa[$val])) {
            $sisa = $sisa - $piut_sisa[$val];
          }

          if ($sisa > 0) {
            $total[$val] = (!empty($total[$val]))?$total[$val]+$sisa:$sisa;
          }else{
            $total[$val] = (!empty($total[$val]))?$total[$val]+0:0;
          }
        }
      }
    }
    foreach ($counter_semester as $key => $val) {
      $hasil[] = array('name' => $val, 'y' => (int)$total[$val]);
    }

    echo json_encode($hasil);
  }

?>
