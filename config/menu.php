  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <header>&emsp;</header>
        <li>
          <a href="index.php">
            <i class="fa fa-home"></i> <span>Dashboard</span>
          </a>
        </li>
        <?php if ($_SESSION['level'] != '2'): ?>
          <li>
            <a href="siswa_view.php">
              <i class="fa fa-user"></i> <span>Data Siswa</span>
            </a>
          </li>
        <?php endif; ?>
        <?php if ($_SESSION['level'] != '2' && $_SESSION['level'] != '4'): ?>
          <li>
            <a href="kelas_view.php">
              <i class="fa fa-table"></i> <span>Data Kelas</span>
            </a>
          </li>
        <?php endif; ?>
        <?php if ($_SESSION['level'] != '2' && $_SESSION['level'] != '4'): ?>
          <li>
            <a href="tahun_ajaran_view.php">
              <i class="fa fa-calendar"></i> <span>Tahun Ajaran</span>
            </a>
          </li>
        <?php endif; ?>
        <?php if ($_SESSION['level'] != '2' && $_SESSION['level'] != '4'): ?>
          <li>
            <a href="pembayaran_view.php">
              <i class="fa fa-money"></i> <span>Pembayaran</span>
            </a>
          </li>
        <?php endif; ?>
        <?php //if ($_SESSION['level'] != '4'): ?>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-file-pdf-o"></i> <span>Laporan</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu" style="display: none;">
              <li><a href="laporan_pembayaran.php"><i class="fa fa-circle-o"></i> Laporan Pembayaran</a></li>
              <li><a href="laporan_piutang.php"><i class="fa fa-circle-o"></i> Laporan Piutang</a></li>
            </ul>
          </li>
        <?php //endif; ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
