<?php
  include 'config/header.php';
  include 'config/menu.php';

  $arr_bulan = array(
      '01' => 'Januari',
      '02' => 'Februari',
      '03' => 'Maret',
      '04' => 'April',
      '05' => 'Mei',
      '06' => 'Juni',
      '07' => 'Juli',
      '08' => 'Agustus',
      '09' => 'September',
      '10' => 'Oktober',
      '11' => 'November',
      '12' => 'Desember'
  );
  $res_th_ajaran = mysqli_query($koneksi, "SELECT * FROM tb_tahun_ajaran");

  $login_is_siswa = "";
  if ($_SESSION['level'] == '4') {
    $login_is_siswa = "WHERE s.nis = '".$_SESSION['id']."'";
  }
?>
  <script src="bower_components/select2/dist/js/select2.full.min.js"></script>
  <script src="plugins/highcharts/code/highcharts.js"></script>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <div class="pull-right">
            <div style="width: 225px; display: inline-block;">
              <select class="form-control select2" id="id_tahun_ajaran" data-placeholder="Pilih Tahun Ajaran" style="width: 100%">
                <option value=""></option>
                <?php while ($r_ajaran = mysqli_fetch_array($res_th_ajaran, MYSQLI_ASSOC)) { ?>
                <option value="<?php echo $r_ajaran['id_tahun_ajaran']; ?>">
                  <?php echo $r_ajaran['nama_tahun_ajaran']." - ".$r_ajaran['semester']; ?>
                </option>
                <?php } ?>
              </select>
            </div>
            <button type="button" id="tampil" class="btn btn-primary">Tampil</button>
          </div>
        </div>
        <div class="box-body">
          <div id="container_bar_admin">

          </div>
        </div>
      </div>
      <!-- /.box -->

    </section>
  </div>

  <script type="text/javascript">
    function view_bar_admin(datas){
      Highcharts.chart('container_bar_admin', {
          chart: {
              type: 'column'
          },
          title: {
              text: 'Grafik Piutang Per Tahun Ajaran'
          },
          subtitle: {
              text: ''
          },
          xAxis: {
              type: 'category'
          },
          yAxis: {
              title: {
                  text: ''
              }
          },
          legend: {
              enabled: false
          },
          plotOptions: {
              series: {
                  borderWidth: 0,
                  dataLabels: {
                      enabled: true,
                      format: 'Rp. {point.y}'
                  }
              }
          },
          tooltip: {
              headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
              pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>Rp. {point.y}</b><br/>'
          },
          "series": [
              {
                  "name": "Periode",
                  "colorByPoint": true,
                  "data": datas
              }
          ],
      });
    }

    $(function () {
      $('.select2').select2();
      $('#tampil').on('click', function() {
        $.ajax({
          type: "POST",
          url: 'config/ajax.php?aksi=chart_bar_admin',
          dataType: 'json',
          data: {datas: {id_tahun_ajaran : $('#id_tahun_ajaran').val(), where_tambahan : "<?php echo $login_is_siswa; ?>"}},
          success: function(data){
            view_bar_admin(data)
          }
        });
      })
    });
  </script>
<?php
  include 'config/footer.php';
?>
