<?php
  include 'config/header.php';
  include 'config/menu.php';

  $aksi = $_GET['aksi'];
  if ($aksi == 'edit') {
    $getID = $_GET['id'];
    $result = mysqli_query($koneksi, "SELECT * FROM tb_kelas WHERE kode_kelas = '$getID'");

    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

    $kelas = $row['kelas'];
    $tingkatan = $row['tingkatan'];
  }
?>

  <script src="bower_components/select2/dist/js/select2.full.min.js"></script>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        <?php echo ($aksi=='add')?'Tambah':'Ubah'; ?> Kelas
      </h1>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-body">
          <form action="kelas_control.php?aksi=<?php echo $aksi; ?>" method="post" role="form" autocomplete="off">
            <input type="hidden" name="kode_kelas" value="<?php echo ($aksi=='edit')?$getID:''; ?>">
            <div class="row">
              <div class="col-md-8">
                <div class="form-group">
                  <label>Tingkatan Kelas</label>
                  <input type="text" class="form-control" name="tingkatan" value="<?php echo ($aksi=='edit')?$tingkatan:''; ?>">
                </div>
                <div class="form-group">
                  <label>Nama Kelas</label>
                  <input type="text" class="form-control" name="kelas" value="<?php echo ($aksi=='edit')?$kelas:''; ?>">
                </div>
                <div class="pull-right">
                  <button type="submit" class="btn btn-primary">SIMPAN</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
  </div>

  <script type="text/javascript">
    $(function () {
      $('.select2').select2()
    })
  </script>
<?php
  include 'config/footer.php';
?>
