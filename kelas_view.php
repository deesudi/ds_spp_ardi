<?php
  include 'config/header.php';
  include 'config/menu.php';

  $result = mysqli_query($koneksi, "SELECT * FROM tb_kelas");
?>
  <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Data Kelas
      </h1>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-header">
          <div class="pull-right">
            <a href="kelas_form.php?aksi=add" class="btn btn-primary">
              TAMBAH
            </a>
          </div>
        </div>
        <div class="box-body">
          <table id="example1" class="table table-bordered">
            <thead>
              <tr>
                <th>Tingkatan Kelas</th>
                <th>Nama Kelas</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){ ?>
                <tr>
                  <td><?php echo $row['tingkatan']; ?></td>
                  <td><?php echo $row['kelas']; ?></td>
                  <td align="center">
                    <a href="kelas_form.php?aksi=edit&id=<?php echo $row['kode_kelas']; ?>" class="btn btn-sm btn-warning">
                      <i class="fa fa-pencil"></i>
                    </a>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </section>
  </div>

  <script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script type="text/javascript">
    $(function () {
      $('#example1').DataTable({
        'paging'      : false,
        'lengthChange': false,
        'searching'   : true,
        'ordering'    : false,
        'info'        : false,
        'autoWidth'   : false
      })
    })
  </script>
<?php
  include 'config/footer.php';
?>
