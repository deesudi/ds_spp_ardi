<?php
  include 'config/header.php';
  include 'config/menu.php';
  $nis = "";
  $tambahan = "";
  $tgl_awal = "";
  $tgl_akhir = "";

  $login_siswa = "";
  if ($_SESSION['level'] == '4') {
    $login_siswa = "AND b.nis = '".$_SESSION['id']."'";
  }

  if (!empty($_GET)) {
    $nis = $_GET['nis'];
    $tgl_awal = $_GET['tgl_awal'];
    $tgl_akhir = $_GET['tgl_akhir'];

    $tambahan = "WHERE 1=1 ";
    if (!empty($nis) && $nis != "all") {
      $tambahan .= "AND b.nis=$nis ";
    }
    if (!empty($tgl_awal)) {
      $tambahan .= "AND DATE(tgl_bayar)>='$tgl_awal' ";
    }
    if (!empty($tgl_akhir)) {
      $tambahan .= "AND DATE(tgl_bayar)<='$tgl_akhir' ";
    }
  }

  $result = mysqli_query($koneksi, "SELECT *, DATE(tgl_bayar) AS tglbayar FROM tb_pembayaran b
    INNER JOIN tb_siswa s
    ON b.nis = s.nis
    $tambahan $login_siswa");

  $res_siswa = mysqli_query($koneksi, "SELECT * FROM tb_siswa b WHERE 1=1 $login_siswa");

?>
  <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script src="bower_components/select2/dist/js/select2.full.min.js"></script>
  <script src="bower_components/moment/min/moment.min.js"></script>
  <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<style media="screen">
  .marginAtas {
    margin-top: 15px
  }
</style>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Laporan Pembayaran SPP
      </h1>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-header">
          <div class="row marginAtas">
            <div class="col-md-4">
              <select class="form-control select2" name="nis" id="nis" style="width: 100%">
                <?php if ($_SESSION['level'] != '4'): ?>  
                <option value="all">SEMUA SISWA</option>
                <?php endif; ?>
                <?php while ($r_siswa = mysqli_fetch_array($res_siswa, MYSQLI_ASSOC)) { ?>
                <option value="<?php echo $r_siswa['nis']; ?>" <?php echo ($nis==$r_siswa['nis'])?'selected':''; ?>>
                  <?php echo $r_siswa['nis']." - ".$r_siswa['nama_siswa']; ?>
                </option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="row marginAtas">
            <div class="col-md-4">
              <input type="text" class="form-control" id="tgl_awal" placeholder="Tanggal Awal" readonly>
              <input type="hidden" id="tgl_awal_db" value="<?php echo (!empty($tgl_awal))?$tgl_awal:''; ?>">
            </div>
          </div>
          <div class="row marginAtas">
            <div class="col-md-4">
              <input type="text" class="form-control" id="tgl_akhir" placeholder="Tanggal Akhir" readonly>
              <input type="hidden" id="tgl_akhir_db" value="<?php echo (!empty($tgl_akhir))?$tgl_akhir:''; ?>">
            </div>
          </div>
          <div class="row marginAtas">
            <div class="col-md-4">
              <div class=" pull-right">
                <a href="javascript:void(0)" id="cetak" class="btn btn-default">
                  CETAK
                </a>
                <a href="javascript:void(0)" id="cari" class="btn btn-primary">
                  CARI
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="box-body marginAtas">
          <table id="example1" class="table table-bordered">
            <thead>
              <tr>
                <th>NIS</th>
                <th>Nama</th>
                <th>Periode</th>
                <th>Jumlah Bayar</th>
                <th>Tanggal Bayar</th>
              </tr>
            </thead>
            <tbody>
              <?php while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){ ?>
                <tr>
                  <td><?php echo $row['nis']; ?></td>
                  <td><?php echo $row['nama_siswa']; ?></td>
                  <td><?php echo $row['periode']; ?></td>
                  <td><?php echo formatMoney($row['jumlah_bayar']); ?></td>
                  <td><?php echo formatTgl($row['tglbayar'], "view"); ?></td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </section>
  </div>

  <?php
    $tgl_awal_tmp = (!empty($tambahan))?formatTgl($tgl_awal, "view"):'';
    $tgl_akhir_tmp = (!empty($tambahan))?formatTgl($tgl_akhir, "view"):'';
  ?>


  <script type="text/javascript">
    $(function () {
      $('#example1').DataTable({
        'paging'      : false,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : false,
        'info'        : false,
        'autoWidth'   : false
      });
      $('.select2').select2();

      //tanggal awal
      <?php
        if(!empty($tgl_awal_tmp)){
          echo "$('#tgl_awal').val('".$tgl_awal_tmp."');";
        }
      ?>

      $('#tgl_awal').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        autoUpdateInput: false,
        <?php echo (!empty($tgl_awal_tmp))?'startDate : "'.$tgl_awal_tmp.'",':''; ?>
        locale: {
            format: 'DD-MM-YYYY',
            cancelLabel: 'Clear',
            daysOfWeek: [
                "Min",
                "Sen",
                "Sel",
                "Rab",
                "Kam",
                "Jum",
                "Sab"
            ],
            monthNames: [
                "Januari",
                "Februari",
                "Maret",
                "April",
                "Mei",
                "Juni",
                "Juli",
                "Agustus",
                "September",
                "Oktober",
                "November",
                "Desember"
            ]
        },
      });
      $('#tgl_awal').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
        $('#tgl_awal_db').val(picker.startDate.format('YYYY-MM-DD'));
      });

      //tanggal akhir
      <?php
        if(!empty($tgl_akhir_tmp)){
          echo "$('#tgl_akhir').val('".$tgl_akhir_tmp."');";
        }
      ?>

      $('#tgl_akhir').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        autoUpdateInput: false,
        <?php echo (!empty($tgl_akhir_tmp))?'startDate : "'.$tgl_akhir_tmp.'",':''; ?>
        locale: {
            format: 'DD-MM-YYYY',
            cancelLabel: 'Clear',
            daysOfWeek: [
                "Min",
                "Sen",
                "Sel",
                "Rab",
                "Kam",
                "Jum",
                "Sab"
            ],
            monthNames: [
                "Januari",
                "Februari",
                "Maret",
                "April",
                "Mei",
                "Juni",
                "Juli",
                "Agustus",
                "September",
                "Oktober",
                "November",
                "Desember"
            ]
        },
      });
      $('#tgl_akhir').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
        $('#tgl_akhir_db').val(picker.startDate.format('YYYY-MM-DD'));
      });

      $(document).on("click", "#cari", function() {
        var nis = $("#nis").val();
        var tgl_awal = $("#tgl_awal_db").val();
        var tgl_akhir = $("#tgl_akhir_db").val();
        var link = "laporan_pembayaran.php?nis="+ nis +"&tgl_awal="+ tgl_awal +"&tgl_akhir="+ tgl_akhir;
        window.location.href = link;
      });

      $(document).on("click", "#cetak", function() {
        var nis = $("#nis").val();
        var tgl_awal = $("#tgl_awal_db").val();
        var tgl_akhir = $("#tgl_akhir_db").val();
        var link = "laporan_pembayaran_cetak.php?nis="+ nis +"&tgl_awal="+ tgl_awal +"&tgl_akhir="+ tgl_akhir;
        window.open(link);
      });
    });
  </script>
<?php
  include 'config/footer.php';
?>
