<?php
  include 'config/header.php';
  include 'config/menu.php';
  $nis = "";
  $tambahan = "";
  $tgl_awal = "";
  $tgl_akhir = "";
  if (!empty($_GET)) {
    $nis = $_GET['nis'];
    $tgl_awal = $_GET['tgl_awal'];
    $tgl_akhir = $_GET['tgl_akhir'];

    $tambahan = "WHERE 1=1 ";
    if (!empty($nis) && $nis != "all") {
      $tambahan .= "AND b.nis=$nis ";
    }
    if (!empty($tgl_awal)) {
      $tambahan .= "AND DATE(tgl_bayar)>='$tgl_awal' ";
    }
    if (!empty($tgl_akhir)) {
      $tambahan .= "AND DATE(tgl_bayar)<='$tgl_akhir' ";
    }
  }

  $result = mysqli_query($koneksi, "SELECT *, DATE(tgl_bayar) AS tglbayar FROM tb_pembayaran b
    INNER JOIN tb_siswa s
    ON b.nis = s.nis
    $tambahan");

  $res_siswa = mysqli_query($koneksi, "SELECT * FROM tb_siswa WHERE nis = '$nis'");
  $row_siswa = mysqli_fetch_array($res_siswa, MYSQLI_ASSOC);

?>
  <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<style media="screen">
  .marginAtas {
    margin-top: 15px
  }
</style>

  <div class="content-wrapper">
    <section class="content-header">
    </section>
    <section class="content">
      <div class="box">
        <div class="box-header">
          <div class="text-center">
            <h1>
              Laporan Pembayaran SPP<br>
              SMA Candimas Pancasari
            </h1>
          </div>
          <br>
          <table>
            <tr>
              <th width="110px;">NIS</th>
              <th>: <?php echo (!empty($nis) && $nis != "all")?$row_siswa['nis']:'SEMUA SISWA'; ?></th>
            </tr>
            <tr>
              <th>Nama</th>
              <th>: <?php echo (!empty($nis) && $nis != "all")?$row_siswa['nama_siswa']:'SEMUA SISWA'; ?></th>
            </tr>
            <tr>
              <th>Tanggal Awal</th>
              <th>: <?php echo (!empty($tgl_awal))?formatTgl($tgl_awal, "view"):'-'; ?></th>
            </tr>
            <tr>
              <th>Tanggal Akhir</th>
              <th>: <?php echo (!empty($tgl_akhir))?formatTgl($tgl_akhir, "view"):'-'; ?></th>
            </tr>
          </table>
        </div>
        <div class="box-body marginAtas">
          <table id="example1" class="table table-bordered">
            <thead>
              <tr>
                <th>NIS</th>
                <th>Nama</th>
                <th>Periode</th>
                <th>Jumlah Bayar</th>
                <th>Tanggal Bayar</th>
              </tr>
            </thead>
            <tbody>
              <?php while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){ ?>
                <tr>
                  <td><?php echo $row['nis']; ?></td>
                  <td><?php echo $row['nama_siswa']; ?></td>
                  <td><?php echo $row['periode']; ?></td>
                  <td><?php echo formatMoney($row['jumlah_bayar']); ?></td>
                  <td><?php echo formatTgl($row['tglbayar'], "view"); ?></td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </section>
  </div>
  
  <script type="text/javascript">
    $(function () {
      $('#example1').DataTable({
        'paging'      : false,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : false,
        'info'        : false,
        'autoWidth'   : false
      });

      window.print();
    });
  </script>
<?php
  include 'config/footer.php';
?>
