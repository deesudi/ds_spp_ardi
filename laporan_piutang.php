<?php
  include 'config/header.php';
  include 'config/menu.php';
  $nis = "";
  $tambah = array('nis' => '', 'th_ajaran' => '');
  $th_ajaran = "";

  $login_siswa = "";
  if ($_SESSION['level'] == '4') {
    $login_siswa = "AND s.nis = '".$_SESSION['id']."'";
  }

  if (!empty($_GET)) {
    $nis = $_GET['nis'];
    $th_ajaran = $_GET['th_ajaran'];

    if (!empty($nis) && $nis != "all") {
      $tambah['nis'] = "AND nis = $nis ";
    }
    if (!empty($th_ajaran) && $th_ajaran != "all") {
      $tambah['th_ajaran'] = "WHERE id_tahun_ajaran = $th_ajaran ";
    }
  }

  $res_siswa = mysqli_query($koneksi, "SELECT * FROM tb_siswa s WHERE 1=1 $login_siswa");
  $res_th_ajaran = mysqli_query($koneksi, "SELECT * FROM tb_tahun_ajaran");

  $loop_siswa = mysqli_query($koneksi, "SELECT * FROM tb_siswa s WHERE 1=1 $login_siswa ".$tambah['nis']);

  $arr_periode_gan = array(
    "Juli" => "Juli",
    "Agustus" => "Agustus",
    "September" => "September",
    "Oktober" => "Oktober",
    "November" => "November",
    "Desember" => "Desember");
  $arr_periode_gen = array(
    "Januari" => "Januari",
    "Februari" => "Februari",
    "Maret" => "Maret",
    "April" => "April",
    "Mei" => "Mei",
    "Juni" => "Juni");
?>
  <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script src="bower_components/select2/dist/js/select2.full.min.js"></script>
  <script src="bower_components/moment/min/moment.min.js"></script>
  <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

<style media="screen">
  .marginAtas {
    margin-top: 15px
  }
</style>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Laporan Piutang SPP
      </h1>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-header">
          <div class="row marginAtas">
            <div class="col-md-4">
              <select class="form-control select2" name="nis" id="nis" style="width: 100%">
                <?php if ($_SESSION['level'] != '4'): ?>
                <option value="all">SEMUA SISWA</option>
                <?php endif; ?>
                <?php while ($r_siswa = mysqli_fetch_array($res_siswa, MYSQLI_ASSOC)) { ?>
                <option value="<?php echo $r_siswa['nis']; ?>" <?php echo ($nis==$r_siswa['nis'])?'selected':''; ?>>
                  <?php echo $r_siswa['nis']." - ".$r_siswa['nama_siswa']; ?>
                </option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="row marginAtas">
            <div class="col-md-4">
              <select class="form-control select2" name="th_ajaran" id="th_ajaran" style="width: 100%">
                <option value="all">SEMUA TAHUN AJARAN</option>
                <?php while ($r_tahun_ajaran = mysqli_fetch_array($res_th_ajaran, MYSQLI_ASSOC)) { ?>
                <option value="<?php echo $r_tahun_ajaran['id_tahun_ajaran']; ?>" <?php echo ($th_ajaran==$r_tahun_ajaran['id_tahun_ajaran'])?'selected':''; ?>>
                  <?php echo $r_tahun_ajaran['nama_tahun_ajaran']." - ".$r_tahun_ajaran['semester']; ?>
                </option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="row marginAtas">
            <div class="col-md-4">
              <div class=" pull-right">
                <a href="javascript:void(0)" id="cetak" class="btn btn-default">
                  CETAK
                </a>
                <a href="javascript:void(0)" id="cari" class="btn btn-primary">
                  CARI
                </a>
              </div>
            </div>
          </div>
        </div>
        <div class="box-body marginAtas">
          <table id="example1" class="table table-bordered">
            <thead>
              <tr>
                <th>NIS</th>
                <th>Nama</th>
                <th>Periode</th>
                <th>Tahun Ajaran</th>
                <th>Semester</th>
                <th>Jumlah Piutang</th>
              </tr>
            </thead>
            <tbody>

              <?php
                while ($row_siswa = mysqli_fetch_array($loop_siswa, MYSQLI_ASSOC)){
                  $loop_th_ajaran = mysqli_query($koneksi, "SELECT * FROM tb_tahun_ajaran ".$tambah['th_ajaran']);
                    while ($row_th_ajaran = mysqli_fetch_array($loop_th_ajaran, MYSQLI_ASSOC)){
                      $piut_sisa = "";
                      $loop_piutang = mysqli_query($koneksi, "SELECT periode, p.jumlah_bayar
                        FROM tb_pembayaran p
                        INNER JOIN tb_tahun_ajaran t
                        ON p.id_tahun_ajaran = t.id_tahun_ajaran
                        AND nis = '".$row_siswa['nis']."' AND t.id_tahun_ajaran = '".$row_th_ajaran['id_tahun_ajaran']."'");
                      while ($row_piutang = mysqli_fetch_array($loop_piutang, MYSQLI_ASSOC)) {
                        $piut_sisa[$row_piutang['periode']] = $row_piutang['jumlah_bayar'];
                      }

                      $counter_semester = '';
                      if ($row_th_ajaran['semester']=='Ganjil') {
                        $counter_semester = $arr_periode_gan;
                      }else{
                        $counter_semester = $arr_periode_gen;
                      }

                      foreach ($counter_semester as $key => $val) {
                        $sisa = $row_th_ajaran['biaya_iuran_komite'];
                        if (!empty($piut_sisa[$val])) {
                          $sisa = $sisa - $piut_sisa[$val];
                        }

                        if ($sisa > 0) {
              ?>

                <tr>
                  <td><?php echo $row_siswa['nis']; ?></td>
                  <td><?php echo $row_siswa['nama_siswa']; ?></td>
                  <td><?php echo $val; ?></td>
                  <td><?php echo $row_th_ajaran['nama_tahun_ajaran']; ?></td>
                  <td><?php echo $row_th_ajaran['semester']; ?></td>
                  <td><?php echo formatMoney($sisa); ?></td>
                </tr>
              <?php }}}} ?>
            </tbody>
          </table>
        </div>
      </div>
    </section>
  </div>

  <script type="text/javascript">
    $(function () {
      $('#example1').DataTable({
        'paging'      : false,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : false,
        'info'        : false,
        'autoWidth'   : false
      });
      $('.select2').select2();

      $(document).on("click", "#cari", function() {
        var nis = $("#nis").val();
        var th_ajaran = $("#th_ajaran").val();
        var link = "laporan_piutang.php?nis="+ nis +"&th_ajaran="+ th_ajaran;
        window.location.href = link;
      });

      $(document).on("click", "#cetak", function() {
        var nis = $("#nis").val();
        var th_ajaran = $("#th_ajaran").val();
        var link = "laporan_piutang_cetak.php?nis="+ nis +"&th_ajaran="+ th_ajaran;
        window.open(link);
      });
    });
  </script>
<?php
  include 'config/footer.php';
?>
