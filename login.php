<?php
  include 'config/koneksi.php';

  if($_SERVER["REQUEST_METHOD"] == "POST") {
    // username and password sent from form
    $myusername = mysqli_real_escape_string($koneksi,$_POST['username']);
    $mypassword = mysqli_real_escape_string($koneksi,$_POST['password']);

    $sql = "SELECT * FROM tb_user WHERE username = '$myusername' AND password = '$mypassword'";
    $result_user = mysqli_query($koneksi,$sql);
    $sql = "SELECT * FROM tb_siswa WHERE nis = '$myusername' AND nis = '$mypassword'";
    $result_siswa = mysqli_query($koneksi,$sql);

    $count_user = mysqli_num_rows($result_user);
    $count_siswa = mysqli_num_rows($result_siswa);

    $hakakses = array('ADMIN' => '1', 'KEPALA' => '2', 'PEGAWAI' => '3', 'SISWA' => '4');

    if($count_user > 0) {
      $row = mysqli_fetch_array($result_user,MYSQLI_ASSOC);
      session_start();
      $_SESSION['id'] = $row['id_user'];
      $_SESSION['username'] = $row['username'];
      $_SESSION['hakakses'] = $row['hakakses'];
      $_SESSION['level'] = $hakakses[strtoupper($row['hakakses'])];
      $_SESSION['status'] = "login";

      header("location: index.php");
    }elseif($count_siswa > 0) {
      $row = mysqli_fetch_array($result_siswa,MYSQLI_ASSOC);
      session_start();
      $_SESSION['id'] = $row['nis'];
      $_SESSION['username'] = $row['nama_siswa'];
      $_SESSION['hakakses'] = 'SISWA';
      $_SESSION['level'] = $hakakses['SISWA'];
      $_SESSION['status'] = "login";

      header("location: index.php");
    }else{
      $error = "Usernama atau Password Salah !";
    }
   }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login Sistem</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <style media="screen">
    .ds-head{
      background-color: white;
      font-size: 30px;
      font-weight: bold;
      text-align: center;
      font-family: 'Source Sans Pro','Helvetica Neue',Helvetica,Arial,sans-serif;
      padding: 10px;
    }
    .ds-logo{
      position: absolute;
      max-width: 85px;
      left: 12px;
    }
  </style>

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="ds-head">
  <img src="assets/logo" class="ds-logo">
  Sistem Pembayaran SPP <br>
  SMA Candimas Pancasari
</div>
<div class="login-box">
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg"></p>

    <form action="" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Username" name="username">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Masuk</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <div class="social-auth-links text-center">
      <p>&nbsp;</p>
      <?php if (!empty($error)): ?>
        <span style="color: red;"><?php echo $error; ?></span>
      <?php endif; ?>
    </div>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>
