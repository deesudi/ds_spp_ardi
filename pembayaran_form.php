<?php
  include 'config/header.php';
  include 'config/menu.php';

  $aksi = $_GET['aksi'];
  if ($aksi == 'edit') {
    $getID = $_GET['id'];
    $result = mysqli_query($koneksi, "SELECT *, DATE(tgl_bayar) AS tglbayar FROM tb_pembayaran WHERE id_pembayaran = '$getID'");

    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

    $id_pembayaran = $row['id_pembayaran'];
    $nis = $row['nis'];
    $id_tahun_ajaran = $row['id_tahun_ajaran'];
    $periode = $row['periode'];
    $jumlah_bayar = $row['jumlah_bayar'];
    $tgl_bayar = $row['tglbayar'];
  }else{
    $result = mysqli_query($koneksi, "SELECT * FROM tb_pembayaran ORDER BY id_pembayaran DESC LIMIT 1");

    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

    if (mysqli_num_rows($result)>0) {
      $id_pembayaran = $row['id_pembayaran']+1;
    }else {
      $id_pembayaran = 1;
    }
  }

  $arr_periode = array(
    "Januari" => "Januari",
    "Februari" => "Februari",
    "Maret" => "Maret",
    "April" => "April",
    "Mei" => "Mei",
    "Juni" => "Juni",
    "Juli" => "Juli",
    "Agustus" => "Agustus",
    "September" => "September",
    "Oktober" => "Oktober",
    "November" => "November",
    "Desember" => "Desember");
  $res_siswa = mysqli_query($koneksi, "SELECT * FROM tb_siswa");
  $res_th_ajaran = mysqli_query($koneksi, "SELECT * FROM tb_tahun_ajaran");

?>

  <script src="bower_components/select2/dist/js/select2.full.min.js"></script>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        <?php echo ($aksi=='add')?'Tambah':'Ubah'; ?> Pembayaran
      </h1>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-body">
          <form action="pembayaran_control.php?aksi=<?php echo $aksi; ?>" method="post" role="form" autocomplete="off">
            <input type="hidden" name="id_pembayaranOld" value="<?php echo ($aksi=='edit')?$getID:''; ?>">
            <div class="row">
              <div class="col-md-8">
                <div class="form-group">
                  <label>Id Pembayaran</label>
                  <input type="text" class="form-control" name="id_pembayaran" value="<?php echo $id_pembayaran; ?>" readonly>
                </div>
                <div class="form-group">
                  <label>Nama Siswa</label>
                  <select class="form-control select2" name="nis" id="nis" data-placeholder="Pilih Siswa" style="width: 100%" onchange="get_periode()">
                    <option value=""></option>
                    <?php while ($r_siswa = mysqli_fetch_array($res_siswa, MYSQLI_ASSOC)) { ?>
                    <option value="<?php echo $r_siswa['nis']; ?>" <?php echo ($aksi=='edit'&&$nis==$r_siswa['nis'])?'selected':''; ?>>
                      <?php echo $r_siswa['nis']." - ".$r_siswa['nama_siswa']; ?>
                    </option>
                    <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Tahun Ajaran</label>
                  <select class="form-control select2" name="id_tahun_ajaran" id="id_tahun_ajaran" data-placeholder="Pilih Tahun Ajaran" style="width: 100%" onchange="get_periode()">
                    <option value=""></option>
                    <?php while ($r_ajaran = mysqli_fetch_array($res_th_ajaran, MYSQLI_ASSOC)) { ?>
                    <option value="<?php echo $r_ajaran['id_tahun_ajaran']; ?>" <?php echo ($aksi=='edit'&&$id_tahun_ajaran==$r_ajaran['id_tahun_ajaran'])?'selected':''; ?>>
                      <?php echo $r_ajaran['nama_tahun_ajaran']." - ".$r_ajaran['semester']; ?>
                    </option>
                    <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Periode</label>
                  <div id="combo_periode">
                    <select class="form-control select2" name="periode" id="periode" data-placeholder="Pilih Periode" style="width: 100%">
                      <option value=""></option>
                      <?php foreach ($arr_periode as $val) { ?>
                      <option value="<?php echo $val; ?>" <?php echo ($aksi=='edit'&&$periode==$val)?'selected':''; ?>><?php echo $val; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label>Jumlah Bayar</label>
                  <input type="text" class="form-control" name="jumlah_bayar" value="<?php echo ($aksi=='edit')?$jumlah_bayar:''; ?>">
                </div>
                <div class="form-group">
                  <label>Tanggal Bayar</label>
                  <input type="text" class="form-control" id="tgl_bayar" value="<?php echo ($aksi=='edit')?formatTgl($tgl_bayar, 'view'):date('d-m-Y'); ?>" readonly>
                  <input type="hidden" name="tgl_bayar" value="<?php echo ($aksi=='edit')?$tgl_bayar.' '.date('H:i:s'):date('Y-m-d H:i:s'); ?>">
                </div>
                <div class="pull-right">
                  <button type="submit" class="btn btn-primary">SIMPAN</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
  </div>

  <script type="text/javascript">
    $(function () {
      prepare_chosen();

      $(document).on('change', '#periode', function() {
        $.ajax({
          type: "POST",
          url: 'config/ajax.php?aksi=jumlah_bayar',
          dataType: 'html',
          data: {datas: {nis : $('#nis').val(), id_tahun_ajaran : $('#id_tahun_ajaran').val(), periode : $(this).val()}},
          success: function(data){
            $('input[name=jumlah_bayar]').val(data);
          }
        });
      });
    });

    function prepare_chosen() {
      $('.select2').select2();
    }

    function get_periode() {
      $.ajax({
        type: "POST",
        url: 'config/ajax.php?aksi=periode_bayar',
        dataType: 'html',
        data: {datas: {nis : $('#nis').val(), id_tahun_ajaran : $('#id_tahun_ajaran').val()}},
        success: function(data){
          $('#combo_periode').html(data);
          prepare_chosen();
        }
      });
    }
  </script>
<?php
  include 'config/footer.php';
?>
