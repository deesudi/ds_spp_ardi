<?php
  include 'config/header.php';
  include 'config/menu.php';

  $result = mysqli_query($koneksi, "SELECT * FROM tb_pembayaran b
    INNER JOIN tb_siswa s
    ON b.nis = s.nis ");
?>
  <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Data Pembayaran
      </h1>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-header">
          <div class="pull-right">
            <a href="pembayaran_form.php?aksi=add" class="btn btn-primary">
              TAMBAH
            </a>
          </div>
        </div>
        <div class="box-body">
          <table id="example1" class="table table-bordered">
            <thead>
              <tr>
                <th width="110px">Id Pembayaran</th>
                <th>NIS</th>
                <th>Nama Siswa</th>
                <th>Jumlah Bayar</th>
                <th>Periode</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){ ?>
                <tr>
                  <td><?php echo $row['id_pembayaran']; ?></td>
                  <td><?php echo $row['nis']; ?></td>
                  <td><?php echo $row['nama_siswa']; ?></td>
                  <td>Rp. <?php echo formatMoney($row['jumlah_bayar']); ?></td>
                  <td><?php echo $row['periode']; ?></td>
                  <td align="center">
                    <a href="pembayaran_form.php?aksi=edit&id=<?php echo $row['id_pembayaran']; ?>" class="btn btn-sm btn-warning">
                      <i class="fa fa-pencil"></i>
                    </a>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </section>
  </div>

  <script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script type="text/javascript">
    $(function () {
      $('#example1').DataTable({
        'paging'      : false,
        'lengthChange': false,
        'searching'   : true,
        'ordering'    : false,
        'info'        : false,
        'autoWidth'   : false
      })
    })
  </script>
<?php
  include 'config/footer.php';
?>
