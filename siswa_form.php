<?php
  include 'config/header.php';
  include 'config/menu.php';

  $aksi = $_GET['aksi'];
  if ($aksi == 'edit') {
    $getID = $_GET['id'];
    $result = mysqli_query($koneksi, "SELECT * FROM tb_siswa WHERE nis = '$getID'");

    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

    $nis = $row['nis'];
    $id_kelas = $row['id_kelas'];
    $nama_siswa = $row['nama_siswa'];
    $tempat_lahir = $row['tempat_lahir'];
    $alamat = $row['alamat'];
    $tgl_lahir = $row['tgl_lahir'];
    $no_tlp = $row['no_tlp'];
    $agama = $row['agama'];
    $jenis_kelamin = $row['jenis_kelamin'];
  }

  $res_kelas = mysqli_query($koneksi, "SELECT * FROM tb_kelas");
?>

  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <script src="bower_components/select2/dist/js/select2.full.min.js"></script>
  <script src="bower_components/moment/min/moment.min.js"></script>
  <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        <?php echo ($aksi=='add')?'Tambah':'Ubah'; ?> Siswa
      </h1>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-body">
          <form action="siswa_control.php?aksi=<?php echo $aksi; ?>" method="post" role="form" autocomplete="off">
            <input type="hidden" name="nisOLD" value="<?php echo ($aksi=='edit')?$getID:''; ?>">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label>NIS</label>
                  <input type="text" class="form-control" name="nis" value="<?php echo ($aksi=='edit')?$nis:''; ?>">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Kelas</label>
                  <select class="form-control select2" name="id_kelas" data-placeholder="" style="width: 100%">
                    <option value=""></option>
                    <?php while ($r_kls = mysqli_fetch_array($res_kelas, MYSQLI_ASSOC)) { ?>
                    <option value="<?php echo $r_kls['kode_kelas']; ?>" <?php echo ($aksi=='edit'&&$id_kelas==$r_kls['kode_kelas'])?'selected':''; ?>><?php echo $r_kls['tingkatan']." ".$r_kls['kelas']; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Nama</label>
                  <input type="text" class="form-control" name="nama_siswa" value="<?php echo ($aksi=='edit')?$nama_siswa:''; ?>">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Tempat Lahir</label>
                  <input type="text" class="form-control" name="tempat_lahir" value="<?php echo ($aksi=='edit')?$tempat_lahir:''; ?>">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Alamat</label>
                  <textarea name="alamat" rows="4" class="form-control"><?php echo ($aksi=='edit')?$alamat:''; ?></textarea>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Tanggal Lahir</label>
                  <div class="input-group">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" class="form-control" id="tgl_lahir" readonly>
                  </div>
                  <input type="hidden" name="tgl_lahir" value="<?php echo ($aksi=='edit')?$tgl_lahir:''; ?>">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>No. Tlp</label>
                  <input type="text" class="form-control" name="no_tlp" value="<?php echo ($aksi=='edit')?$no_tlp:''; ?>">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Agama</label>
                  <select class="form-control select2" name="agama" data-placeholder="" style="width: 100%">
                    <option value=""></option>
                    <option value="Hindu" <?php echo ($aksi=='edit'&&$agama=='Hindu')?'selected':''; ?>>Hindu</option>
                    <option value="Islam" <?php echo ($aksi=='edit'&&$agama=='Islam')?'selected':''; ?>>Islam</option>
                    <option value="Protestan" <?php echo ($aksi=='edit'&&$agama=='Protestan')?'selected':''; ?>>Protestan</option>
                    <option value="Katolik" <?php echo ($aksi=='edit'&&$agama=='Katolik')?'selected':''; ?>>Katolik</option>
                    <option value="Budha" <?php echo ($aksi=='edit'&&$agama=='Budha')?'selected':''; ?>>Budha</option>
                    <option value="Khonghucu" <?php echo ($aksi=='edit'&&$agama=='Khonghucu')?'selected':''; ?>>Khonghucu</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label>Jenis Kelamin</label>
                  <select class="form-control select2" name="jenis_kelamin" data-placeholder="" style="width: 100%">
                    <option value=""></option>
                    <option value="Laki-laki" <?php echo ($aksi=='edit'&&$jenis_kelamin=='Laki-laki')?'selected':''; ?>>Laki-laki</option>
                    <option value="Perempuan" <?php echo ($aksi=='edit'&&$jenis_kelamin=='Perempuan')?'selected':''; ?>>Perempuan</option>
                  </select>
                </div>
              </div>
              <div class="col-md-12">
                <div class="pull-right">
                  <button type="submit" class="btn btn-primary">SIMPAN</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
  </div>
  <?php
    $tglLahirTemp = ($aksi=='edit'&&!empty($tgl_lahir))?formatTgl($tgl_lahir, "view"):'';
  ?>

  <script type="text/javascript">
    $(function () {
      $('.select2').select2();

      <?php
        if($aksi=='edit'&&!empty($tglLahirTemp)){
          echo "$('#tgl_lahir').val('".$tglLahirTemp."');";
        }
      ?>

      $('#tgl_lahir').daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        autoUpdateInput: false,
        <?php echo ($aksi=='edit'&&!empty($tglLahirTemp))?'startDate : "'.$tglLahirTemp.'",':''; ?>
        locale: {
            format: 'DD-MM-YYYY',
            cancelLabel: 'Clear',
            daysOfWeek: [
                "Min",
                "Sen",
                "Sel",
                "Rab",
                "Kam",
                "Jum",
                "Sab"
            ],
            monthNames: [
                "Januari",
                "Februari",
                "Maret",
                "April",
                "Mei",
                "Juni",
                "Juli",
                "Agustus",
                "September",
                "Oktober",
                "November",
                "Desember"
            ]
        },
      });
      $('#tgl_lahir').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY'));
        $('input[name=tgl_lahir]').val(picker.startDate.format('YYYY-MM-DD'));
      });
    })
  </script>
<?php
  include 'config/footer.php';
?>
