<?php
  include 'config/header.php';
  include 'config/menu.php';

  $tambahan = '';
  if ($_SESSION['level'] == '4') {
    $tambahan = "WHERE s.nis = '".$_SESSION['id']."'";
  }

  $result = mysqli_query($koneksi, "SELECT * FROM tb_siswa s
    LEFT JOIN tb_kelas k ON s.id_kelas = k.kode_kelas $tambahan");
?>
  <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Data Siswa
      </h1>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-header">
          <?php if ($_SESSION['level'] != '4'): ?>
            
          <div class="pull-right">
            <a href="siswa_form.php?aksi=add" class="btn btn-primary">
              TAMBAH
            </a>
          </div>

          <?php endif; ?>
        </div>
        <div class="box-body">
          <table id="example1" class="table table-bordered">
            <thead>
              <tr>
                <th>NIS</th>
                <th>Nama</th>
                <th>Alamat</th>
                <th>No. Tlp</th>
                <th>Jenis Kelamin</th>
                <th>Kelas</th>
                <th>Tempat Lahir</th>
                <th>Tanggal Lahir</th>
                <th>Agama</th>
                <?php if ($_SESSION['level'] != '4'): ?>
                <th>Aksi</th>
                <?php endif; ?>
              </tr>
            </thead>
            <tbody>
              <?php while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){ ?>
                <tr>
                  <td><?php echo $row['nis']; ?></td>
                  <td><?php echo $row['nama_siswa']; ?></td>
                  <td><?php echo $row['alamat']; ?></td>
                  <td><?php echo $row['no_tlp']; ?></td>
                  <td><?php echo $row['jenis_kelamin']; ?></td>
                  <td><?php echo $row['kelas']; ?></td>
                  <td><?php echo $row['tempat_lahir']; ?></td>
                  <td><?php echo formatTgl($row['tgl_lahir'], 'view'); ?></td>
                  <td><?php echo $row['agama']; ?></td>
                  <?php if ($_SESSION['level'] != '4'): ?>
                  <td align="center">
                    <a href="siswa_form.php?aksi=edit&id=<?php echo $row['nis']; ?>" class="btn btn-sm btn-warning">
                      <i class="fa fa-pencil"></i>
                    </a>
                  </td>
                  <?php endif; ?>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </section>
  </div>

  <script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script type="text/javascript">
    $(function () {
      $('#example1').DataTable({
        'paging'      : false,
        'lengthChange': false,
        'searching'   : true,
        'ordering'    : false,
        'info'        : false,
        'autoWidth'   : false
      })
    })
  </script>
<?php
  include 'config/footer.php';
?>
