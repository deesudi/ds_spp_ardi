<?php
  include 'config/header.php';
  include 'config/menu.php';

  $aksi = $_GET['aksi'];
  if ($aksi == 'edit') {
    $getID = $_GET['id'];
    $result = mysqli_query($koneksi, "SELECT * FROM tb_tahun_ajaran WHERE id_tahun_ajaran = '$getID'");

    $row = mysqli_fetch_array($result, MYSQLI_ASSOC);

    $nama_tahun_ajaran = $row['nama_tahun_ajaran'];
    $semester = $row['semester'];
    $biaya_iuran_komite = $row['biaya_iuran_komite'];
  }
?>

  <script src="bower_components/select2/dist/js/select2.full.min.js"></script>

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        <?php echo ($aksi=='add')?'Tambah':'Ubah'; ?> Kelas
      </h1>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-body">
          <form action="tahun_ajaran_control.php?aksi=<?php echo $aksi; ?>" method="post" role="form" autocomplete="off">
            <input type="hidden" name="id_tahun_ajaran" value="<?php echo ($aksi=='edit')?$getID:''; ?>">
            <div class="row">
              <div class="col-md-8">
                <div class="form-group">
                  <label>Nama Tahun Ajaran</label>
                  <input type="text" class="form-control" name="nama_tahun_ajaran" value="<?php echo ($aksi=='edit')?$nama_tahun_ajaran:''; ?>">
                </div>
                <div class="form-group">
                  <label>Semester</label>
                  <input type="text" class="form-control" name="semester" value="<?php echo ($aksi=='edit')?$semester:''; ?>">
                </div>
                <div class="form-group">
                  <label>Biaya SPP</label>
                  <input type="text" class="form-control" name="biaya_iuran_komite" value="<?php echo ($aksi=='edit')?$biaya_iuran_komite:''; ?>">
                </div>
                <div class="pull-right">
                  <button type="submit" class="btn btn-primary">SIMPAN</button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </section>
  </div>

  <script type="text/javascript">
    $(function () {
      $('.select2').select2()
    })
  </script>
<?php
  include 'config/footer.php';
?>
