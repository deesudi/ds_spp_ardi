<?php
  include 'config/header.php';
  include 'config/menu.php';

  $result = mysqli_query($koneksi, "SELECT * FROM tb_tahun_ajaran");
?>
  <link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Data Tahun Ajaran
      </h1>
    </section>
    <section class="content">
      <div class="box">
        <div class="box-header">
          <div class="pull-right">
            <a href="tahun_ajaran_form.php?aksi=add" class="btn btn-primary">
              TAMBAH
            </a>
          </div>
        </div>
        <div class="box-body">
          <table id="example1" class="table table-bordered">
            <thead>
              <tr>
                <th>Nama Tahun Ajaran</th>
                <th>Semester</th>
                <th>Biaya SPP</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              <?php while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)){ ?>
                <tr>
                  <td><?php echo $row['nama_tahun_ajaran']; ?></td>
                  <td><?php echo $row['semester']; ?></td>
                  <td>Rp. <?php echo formatMoney($row['biaya_iuran_komite']); ?></td>
                  <td align="center">
                    <a href="tahun_ajaran_form.php?aksi=edit&id=<?php echo $row['id_tahun_ajaran']; ?>" class="btn btn-sm btn-warning">
                      <i class="fa fa-pencil"></i>
                    </a>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
      </div>
    </section>
  </div>

  <script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
  <script type="text/javascript">
    $(function () {
      $('#example1').DataTable({
        'paging'      : false,
        'lengthChange': false,
        'searching'   : true,
        'ordering'    : false,
        'info'        : false,
        'autoWidth'   : false
      })
    })
  </script>
<?php
  include 'config/footer.php';
?>
